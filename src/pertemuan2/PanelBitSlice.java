package pertemuan2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class PanelBitSlice extends JPanel {
	private JLabel labelFrom, labelTo, space, labelTitle;
	private JLabel labelImageFrom, labelImageTo;
	private GridBagLayout layout;
	private GridBagConstraints gbc;
	private Font fontTitle, fontHeader;
	private Dimension dimensionImage;
	private JFileChooser fileChooser;
	private JButton buttonChoose;
	private JScrollBar scrollbarSlice;
	private JPanel panelSetting;
	private JButton buttonSlice;
	private JLabel labelSliceValue;
	private JLabel labelSliceTitle;
	
	private Mat matGreyscale;
	
	public PanelBitSlice() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		
		gbc = new GridBagConstraints();
		
		fontTitle = this.getFont().deriveFont(getFont().getSize() * 2F);
		fontHeader = this.getFont().deriveFont(getFont().getSize() * 1.5F);
		
		dimensionImage = new Dimension(350, 350);
		
		//title label
		labelTitle = new JLabel("Bit Plane Slicing", JLabel.CENTER);
		labelTitle.setFont(fontTitle);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 10, 10);
		this.add(labelTitle, gbc);
		
		
		//label source
		labelFrom = new JLabel("Source Image", JLabel.CENTER);
		labelFrom.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		this.add(labelFrom, gbc);
		
		//add space
		space = new JLabel();
		space.setPreferredSize(new Dimension(100, 0));
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(space, gbc);
		
		//label result
		labelTo = new JLabel("Result Image", JLabel.CENTER);
		labelTo.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 1;
		this.add(labelTo, gbc);
		
		
		
		//label image source
		labelImageFrom = new JLabel();
		labelImageFrom.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 2;
		labelImageFrom.setOpaque(true);
		labelImageFrom.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageFrom, gbc);
		
		//label image result
		labelImageTo = new JLabel();
		labelImageTo.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 2;
		labelImageTo.setOpaque(true);
		labelImageTo.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageTo, gbc);
				
		
		fileChooser = new JFileChooser();
		buttonChoose = new JButton("Choose Image");
		buttonChoose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(PanelBitSlice.this);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
					String filePath = fileChooser.getSelectedFile().getAbsolutePath();
					File file = new File(filePath);
					
					try {
						//set source image
						BufferedImage bufferedImageSource = ImageIO.read(file);
						Image imageFrom = bufferedImageSource.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH); 
						
						labelImageFrom.setIcon(new ImageIcon(imageFrom));
						
						
						byte[] dataSource = ((DataBufferByte) bufferedImageSource.getRaster().getDataBuffer()).getData();
						Mat matSource = new Mat(bufferedImageSource.getHeight(), bufferedImageSource.getWidth(), CvType.CV_8UC3);
						matSource.put(0, 0, dataSource);

						
						//generate greyscale matrix
						matGreyscale = new Mat(bufferedImageSource.getHeight(),bufferedImageSource.getWidth(),CvType.CV_8UC1);
						Imgproc.cvtColor(matSource, matGreyscale, Imgproc.COLOR_RGB2GRAY);
						applyBitSlice();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					
				}
				
			}
		});
		gbc.fill = GridBagConstraints.REMAINDER;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		this.add(buttonChoose, gbc);
		
		
		panelSetting = new JPanel();
		panelSetting.setLayout(null);
		panelSetting.setPreferredSize(new Dimension(350, 100));
		//panelSetting.setBackground(Color.LIGHT_GRAY);
		
		labelSliceTitle = new JLabel("Bit Plane: ");
		labelSliceTitle.setFont(fontHeader);
		labelSliceTitle.setBounds(0, 0, 200, 50);
		panelSetting.add(labelSliceTitle);
		
		labelSliceValue = new JLabel();
		labelSliceValue.setFont(fontHeader);
		labelSliceValue.setBounds(150, 0, 100, 50);
		panelSetting.add(labelSliceValue);
		
		//scrollbar
		scrollbarSlice = new JScrollBar(JScrollBar.HORIZONTAL);
		scrollbarSlice.setMaximum(90);
		scrollbarSlice.setMinimum(10);
		scrollbarSlice.setValue(10);
		scrollbarSlice.setUnitIncrement(10);
		scrollbarSlice.addAdjustmentListener(new AdjustmentListener() {
			
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				labelSliceValue.setText(String.valueOf(scrollbarSlice.getValue()/10));
			}
		});
		scrollbarSlice.setBounds(0, 60, 200, 35);
		panelSetting.add(scrollbarSlice);
		
		labelSliceValue.setText(String.valueOf(scrollbarSlice.getValue()/10));
		
		
		buttonSlice = new JButton("APPLY");
		buttonSlice.setBounds(220, 30, 100, 50);
		buttonSlice.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				applyBitSlice();
			}
		});
		panelSetting.add(buttonSlice);
		
		
		gbc.fill = GridBagConstraints.REMAINDER;
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		this.add(panelSetting, gbc);
		panelSetting.setVisible(false);
		
	}
	
	private void applyBitSlice() {
		Mat matDest = new Mat(matGreyscale.cols(), matGreyscale.rows(), CvType.CV_8UC1);
		
		//matDest sudah greyscale
		//buat matrix dengan value sesuai bit plane
		int value = 1 << (scrollbarSlice.getValue()/10 - 1);
		Mat matSlice = new Mat(matDest.cols(), matDest.rows(), CvType.CV_8UC1, new Scalar(value));
		if (matDest.cols() != matGreyscale.cols()) {
			matDest = new Mat(matGreyscale.rows(), matGreyscale.cols(), CvType.CV_8UC1);
			matSlice = new Mat(matGreyscale.rows(), matGreyscale.cols(), CvType.CV_8UC1, new Scalar(value));
		}
		
		
		//AND-kan slice dengan greyscale
		Core.bitwise_and(matGreyscale, matSlice, matDest);
		
		
		//show the image
		byte[] dataDest = new byte[matDest.rows() * matDest.cols() * (int)(matDest.elemSize())];
		matDest.get(0, 0, dataDest);
		BufferedImage bufferedImageDest = new BufferedImage(matDest.cols(),matDest.rows(), BufferedImage.TYPE_BYTE_GRAY);
		bufferedImageDest.getRaster().setDataElements(0, 0, matDest.cols(), matDest.rows(), dataDest);
		Image imageTo = bufferedImageDest.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH);
		
		labelImageTo.setIcon(new ImageIcon(imageTo));
		
		panelSetting.setVisible(true);
	}
}
