package pertemuan1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class PanelGreyscale extends JPanel {
	private JLabel labelFrom, labelTo, space, labelTitle;
	private JLabel labelImageFrom, labelImageTo;
	private GridBagLayout layout;
	private GridBagConstraints gbc;
	private Font fontTitle, fontHeader;
	private Dimension dimensionImage;
	private JFileChooser fileChooser;
	private JButton buttonChoose;
	
	public PanelGreyscale() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		
		gbc = new GridBagConstraints();
		
		fontTitle = this.getFont().deriveFont(getFont().getSize() * 2F);
		fontHeader = this.getFont().deriveFont(getFont().getSize() * 1.5F);
		
		dimensionImage = new Dimension(350, 350);
		
		//title label
		labelTitle = new JLabel("Greyscale", JLabel.CENTER);
		labelTitle.setFont(fontTitle);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 10, 10);
		this.add(labelTitle, gbc);
		
		
		//label source
		labelFrom = new JLabel("Source Image", JLabel.CENTER);
		labelFrom.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		this.add(labelFrom, gbc);
		
		//add space
		space = new JLabel();
		space.setPreferredSize(new Dimension(100, 0));
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(space, gbc);
		
		//label result
		labelTo = new JLabel("Result Image", JLabel.CENTER);
		labelTo.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 1;
		this.add(labelTo, gbc);
		
		
		
		//label image source
		labelImageFrom = new JLabel();
		labelImageFrom.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 2;
		labelImageFrom.setOpaque(true);
		labelImageFrom.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageFrom, gbc);
		
		//label image result
		labelImageTo = new JLabel();
		labelImageTo.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 2;
		labelImageTo.setOpaque(true);
		labelImageTo.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageTo, gbc);
				
		
		fileChooser = new JFileChooser();
		buttonChoose = new JButton("Choose Image");
		buttonChoose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(PanelGreyscale.this);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
					String filePath = fileChooser.getSelectedFile().getAbsolutePath();
					File file = new File(filePath);
					
					try {
						//set source image
						BufferedImage bufferedImageSource = ImageIO.read(file);
						Image imageFrom = bufferedImageSource.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH); 
						
						labelImageFrom.setIcon(new ImageIcon(imageFrom));
						
						
						//generate greyscale image
						byte[] dataSource = ((DataBufferByte) bufferedImageSource.getRaster().getDataBuffer()).getData();
						Mat matSource = new Mat(bufferedImageSource.getHeight(), bufferedImageSource.getWidth(), CvType.CV_8UC3);
						matSource.put(0, 0, dataSource);

						Mat matDest = new Mat(bufferedImageSource.getHeight(),bufferedImageSource.getWidth(),CvType.CV_8UC1);
						Imgproc.cvtColor(matSource, matDest, Imgproc.COLOR_RGB2GRAY);

						byte[] dataDest = new byte[matDest.rows() * matDest.cols() * (int)(matDest.elemSize())];
						matDest.get(0, 0, dataDest);
						BufferedImage bufferedImageDest = new BufferedImage(matDest.cols(),matDest.rows(), BufferedImage.TYPE_BYTE_GRAY);
						bufferedImageDest.getRaster().setDataElements(0, 0, matDest.cols(), matDest.rows(), dataDest);
						Image imageTo = bufferedImageDest.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH);
						
						labelImageTo.setIcon(new ImageIcon(imageTo));
						
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					
				}
				
			}
		});
		gbc.fill = GridBagConstraints.REMAINDER;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		this.add(buttonChoose, gbc);
		
	}
}
