package main;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import pertemuan1.PanelGreyscale;
import pertemuan2.PanelBitSlice;
import pertemuan2.PanelNegative;
import pertemuan2.PanelThreshold;
import pertemuan3.PanelCustomFilter;
import pertemuan3.PanelMinMaxMedian;

public class Main extends JFrame implements ActionListener {
	private JPanel panelMain;
	private JMenuBar menuBar;
	private JMenu menuFile, menuFeatures, menuPertemuan1, menuPertemuan2, menuPertemuan3;
	private JMenuItem menuItemExit, menuItemGreyscale, menuItemHistogram, menuItemNegative, menuItemThreshold, menuItemBitSlice, menuItemMinMaxMedian, menuItemCustomFilter;
	private CardLayout cardLayoutMain;
	private JPanel panelFirst, panelGreyscale, panelNegative, panelThreshold, panelBitSlice, panelMinMaxMedian, panelCustomFilter;
				
	
	
	public Main() {
		this.setTitle("Image Processing by Charlie");
		this.setSize(1000, 700);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		//set menubar
		menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		//file section
		menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menuFile);
		
		//exit menu
		menuItemExit = new JMenuItem("Exit");
		menuItemExit.setMnemonic(KeyEvent.VK_X);
		menuItemExit.addActionListener(this);
		menuFile.add(menuItemExit);
		
		
		//features section
		menuFeatures = new JMenu("Features");
		menuFeatures.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menuFeatures);
		
		
		//pertemuan1 submenu
		menuPertemuan1 = new JMenu("Pertemuan 1");
		menuFeatures.add(menuPertemuan1);
		
		//pertemuan1 - greyscale item
		menuItemGreyscale = new JMenuItem("Greyscale");
		menuItemGreyscale.addActionListener(this);
		menuPertemuan1.add(menuItemGreyscale);
		
		//pertemuan1 - histogram item
		menuItemHistogram = new JMenuItem("Histogram");
		menuItemHistogram.addActionListener(this);
		//menuPertemuan1.add(menuItemHistogram);
		

		//pertemuan2 submenu
		menuPertemuan2 = new JMenu("Pertemuan 2");
		menuFeatures.add(menuPertemuan2);
		
		//pertemuan2 - negative
		menuItemNegative = new JMenuItem("Negative");
		menuItemNegative.addActionListener(this);
		menuPertemuan2.add(menuItemNegative);
		
		//pertemuan2 - threshold
		menuItemThreshold = new JMenuItem("Thresholding");
		menuItemThreshold.addActionListener(this);
		menuPertemuan2.add(menuItemThreshold);
		
		//pertemuan2 - Bit Plane Slicing
		menuItemBitSlice = new JMenuItem("Bit Plane Slicing");
		menuItemBitSlice.addActionListener(this);
		menuPertemuan2.add(menuItemBitSlice);
		
		
		//pertemuan1 submenu
		menuPertemuan3 = new JMenu("Pertemuan 3");
		menuFeatures.add(menuPertemuan3);
				
		
		//pertemuan3 - Min Max Median
		menuItemMinMaxMedian = new JMenuItem("Min, Max, Median");
		menuItemMinMaxMedian.addActionListener(this);
		menuPertemuan3.add(menuItemMinMaxMedian);
		
		//pertemuan3 - Custom Filter
		menuItemCustomFilter = new JMenuItem("Custom Filter");
		menuItemCustomFilter.addActionListener(this);
		menuPertemuan3.add(menuItemCustomFilter);
		
		
		
		//set panel
		panelMain = new JPanel();
		this.setContentPane(panelMain);
		
		//set layout
		cardLayoutMain = new CardLayout();
		panelMain.setLayout(cardLayoutMain);
		
		
		
		//start panel
		panelFirst = new PanelFirst();
		panelMain.add("first", panelFirst);
		
		
		//panel greyscale
		panelGreyscale = new PanelGreyscale();
		panelMain.add("greyscale", panelGreyscale);
		
		
		//panel negative
		panelNegative = new PanelNegative();
		panelMain.add("negative", panelNegative);
		
		//panel threshold
		panelThreshold = new PanelThreshold();
		panelMain.add("threshold", panelThreshold);
		
		//panel bit plane slice
		panelBitSlice = new PanelBitSlice();
		panelMain.add("bitslice", panelBitSlice);
		
		//panel min max median
		panelMinMaxMedian = new PanelMinMaxMedian();
		panelMain.add("minmaxmedian", panelMinMaxMedian);
		
		//panel custom filter
		panelCustomFilter = new PanelCustomFilter();
		panelMain.add("customfilter", panelCustomFilter);
		
		//cardLayoutMain.show(panelMain, "bitslice");
		
		//set all components to visible
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Main();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//handle menu action
		if (e.getSource() == menuItemExit) {
			System.exit(0);
		} else if (e.getSource() == menuItemGreyscale) {
			cardLayoutMain.show(panelMain, "greyscale");
		} else if (e.getSource() == menuItemNegative) {
			cardLayoutMain.show(panelMain, "negative");
		} else if (e.getSource() == menuItemThreshold) {
			cardLayoutMain.show(panelMain, "threshold");
		} else if (e.getSource() == menuItemBitSlice) {
			cardLayoutMain.show(panelMain, "bitslice");
		} else if (e.getSource() == menuItemMinMaxMedian) {
			cardLayoutMain.show(panelMain, "minmaxmedian");
		} else if (e.getSource() == menuItemCustomFilter) {
			cardLayoutMain.show(panelMain, "customfilter");
		}
		
	}
}
