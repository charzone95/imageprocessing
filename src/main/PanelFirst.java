package main;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class PanelFirst extends JPanel {
	private JLabel labelText;
	
	public PanelFirst() {
		//this.setLayout(new BoxLayout());
		this.setLayout(new GridBagLayout());
		labelText = new JLabel("Choose the features above...");
		labelText.setFont(getFont().deriveFont(getFont().getSize() * 2F));
		
		this.add(labelText);
	}
}
