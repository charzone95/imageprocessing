package pertemuan3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class PanelMinMaxMedian extends JPanel {
	private JLabel labelFrom, labelTo, space, labelTitle;
	private JLabel labelImageFrom, labelImageTo;
	private GridBagLayout layout;
	private GridBagConstraints gbc;
	private Font fontTitle, fontHeader;
	private Dimension dimensionImage;
	private JFileChooser fileChooser;
	private JButton buttonChoose;
	private JPanel panelSetting;
	private JLabel labelModeTitle;
	private ButtonGroup groupRadio;
	private JRadioButton radioMin, radioMax, radioMedian;
	private Mat matGreyscale;
	
	public PanelMinMaxMedian() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		
		gbc = new GridBagConstraints();
		
		fontTitle = this.getFont().deriveFont(getFont().getSize() * 2F);
		fontHeader = this.getFont().deriveFont(getFont().getSize() * 1.5F);
		
		dimensionImage = new Dimension(350, 350);
		
		//title label
		labelTitle = new JLabel("Min, Max, Median", JLabel.CENTER);
		labelTitle.setFont(fontTitle);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		gbc.insets = new Insets(10, 10, 10, 10);
		this.add(labelTitle, gbc);
		
		
		//label source
		labelFrom = new JLabel("Source Image (Gray)", JLabel.CENTER);
		labelFrom.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		this.add(labelFrom, gbc);
		
		//add space
		space = new JLabel();
		space.setPreferredSize(new Dimension(100, 0));
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(space, gbc);
		
		//label result
		labelTo = new JLabel("Result Image", JLabel.CENTER);
		labelTo.setFont(fontHeader);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 1;
		this.add(labelTo, gbc);
		
		
		
		//label image source
		labelImageFrom = new JLabel();
		labelImageFrom.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 2;
		labelImageFrom.setOpaque(true);
		labelImageFrom.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageFrom, gbc);
		
		//label image result
		labelImageTo = new JLabel();
		labelImageTo.setPreferredSize(dimensionImage);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 2;
		gbc.gridy = 2;
		labelImageTo.setOpaque(true);
		labelImageTo.setBackground(Color.LIGHT_GRAY);
		this.add(labelImageTo, gbc);
				
		
		fileChooser = new JFileChooser();
		buttonChoose = new JButton("Choose Image");
		buttonChoose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(PanelMinMaxMedian.this);
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
					String filePath = fileChooser.getSelectedFile().getAbsolutePath();
					File file = new File(filePath);
					
					try {
						//set source image
						BufferedImage bufferedImageSource = ImageIO.read(file);
						
						Mat matSource = Imgcodecs.imread(filePath);

						
						//generate greyscale matrix
						matGreyscale = new Mat(matSource.rows(), matSource.cols(), CvType.CV_8UC1);
						Imgproc.cvtColor(matSource, matGreyscale, Imgproc.COLOR_RGB2GRAY);
						
						//Image imageFrom = bufferedImageSource.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH); 
						byte[] dataGrey = new byte[matGreyscale.rows() * matGreyscale.cols() * (int)(matGreyscale.elemSize())];
						matGreyscale.get(0, 0, dataGrey);
						BufferedImage bufferedImageDest = new BufferedImage(matGreyscale.cols(),matGreyscale.rows(), BufferedImage.TYPE_BYTE_GRAY);
						bufferedImageDest.getRaster().setDataElements(0, 0, matGreyscale.cols(), matGreyscale.rows(), dataGrey);
						Image imageFrom = bufferedImageDest.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH);
						
						labelImageFrom.setIcon(new ImageIcon(imageFrom));
						
						
						applyProcess();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					
				}
				
			}
		});
		gbc.fill = GridBagConstraints.REMAINDER;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		this.add(buttonChoose, gbc);
		
		
		panelSetting = new JPanel();
		panelSetting.setLayout(null);
		panelSetting.setPreferredSize(new Dimension(200, 100));
		//panelSetting.setBackground(Color.LIGHT_GRAY);
		
		labelModeTitle = new JLabel("Filter Mode: ");
		labelModeTitle.setFont(fontHeader);
		labelModeTitle.setBounds(0, 0, 220, 50);
		panelSetting.add(labelModeTitle);
		
		groupRadio = new ButtonGroup();
		
		//radio min
		radioMin = new JRadioButton("Min");
		radioMin.setBounds(0, 60, 50, 35);
		radioMin.setSelected(true);
		radioMin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				applyProcess();
			}
		});
		groupRadio.add(radioMin);
		panelSetting.add(radioMin);
		
		//radio min
		radioMax = new JRadioButton("Max");
		radioMax.setBounds(60, 60, 50, 35);
		radioMax.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				applyProcess();
			}
		});
		groupRadio.add(radioMax);
		panelSetting.add(radioMax);
		
		//radio min
		radioMedian = new JRadioButton("Median");
		radioMedian.setBounds(120, 60, 80, 35);
		radioMedian.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				applyProcess();
			}
		});
		groupRadio.add(radioMedian);
		panelSetting.add(radioMedian);
		
		
		
		gbc.fill = GridBagConstraints.REMAINDER;
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		this.add(panelSetting, gbc);
		panelSetting.setVisible(false);
		
	}
	
	private void applyProcess() {
		Mat matDest = new Mat(matGreyscale.rows(), matGreyscale.cols(), CvType.CV_8UC1);
		
		
		//double for untuk jalan2 ke imagenya
		for (int i=1;i<matDest.rows()-1;i++) {
			for (int j=1;j<matDest.cols()-1;j++) {
				ArrayList<Integer> values = new ArrayList<>();
				
				//double for untuk ambil matriks 3x3
				for (int k=i-1;k<=i+1;k++) {
					for (int l=j-1;l<=j+1;l++) {
						values.add((int)(matGreyscale.get(k, l)[0]));
						//System.out.println(k + " " + l + " " + matGreyscale.get(k, l));
					}
				}
				
				Collections.sort(values);
				
				//min -> array ke 0
				int selected = 0;
				
				if (radioMin.isSelected()) {
					selected = 0;
				} else if (radioMax.isSelected()) {
					selected = values.size() - 1;
				} else {
					selected = values.size() / 2;
				}
				matDest.put(i, j, values.get(selected));
			}
		}
		
		//copy border pixels
		for (int i=0;i<matDest.rows();i++) {
			int x = i;
			int y = 1;
			
			if (i==0) {
				x = 1;
			} else if (i == matDest.rows() - 1) {
				x = matDest.rows() - 2;
			}
			
			matDest.put(i, y-1, matDest.get(x, y));
			
			
			y = matDest.cols()-2;
			matDest.put(i, y+1, matDest.get(x, y));
			
		}	
		for (int i=0;i<matDest.rows();i++) {
			int x = i;
			int y = 1;
			
			if (i==0) {
				x = 1;
			} else if (i == matDest.rows() - 1) {
				x = matDest.rows() - 2;
			}
			
			matDest.put(y-1, i, matDest.get(y, x));
			
			
			y = matDest.cols()-2;
			matDest.put(y+1, i, matDest.get(y, x));
			
		}
		
		
		
		
		//show the image
		byte[] dataDest = new byte[matDest.rows() * matDest.cols() * (int)(matDest.elemSize())];
		matDest.get(0, 0, dataDest);
		BufferedImage bufferedImageDest = new BufferedImage(matDest.cols(),matDest.rows(), BufferedImage.TYPE_BYTE_GRAY);
		bufferedImageDest.getRaster().setDataElements(0, 0, matDest.cols(), matDest.rows(), dataDest);
		Image imageTo = bufferedImageDest.getScaledInstance(dimensionImage.width, dimensionImage.height, Image.SCALE_SMOOTH);
			
		labelImageTo.setIcon(new ImageIcon(imageTo));
		
		panelSetting.setVisible(true);
	}
}
